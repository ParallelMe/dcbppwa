#!/bin/bash

inFrame ()
{ #message printing function
echo "=================================================================="
echo -e ${1}
echo "=================================================================="
}

choseDev ()
{
select dev in "cpu" "gpu" "phi"
do
#   echo "You have chosen $dev"
  break  # если 'break' убрать, то получится бесконечный цикл.
done
}

runMes ()
{
inFrame "Running code on ${1}..."
}
