#!/bin/bash

TARGET=test_JpsiToKKpi0

# export OMP_PLACES=cores

# THR=48
# export OMP_NUM_THREADS=$THR
# # make $TARGET && srun -p tut -t 720 -n 1 -c $THR ./$TARGET
# make $TARGET && srun -p interactive -t 720 -n 1 -c $THR ./$TARGET
# # make $TARGET && srun -p cpu -t 720 -n 1 -c $THR ./$TARGET

THR=48
export OMP_NUM_THREADS=$THR
# make $TARGET && srun -p interactive -t 720 --gres=gpu:1 ./$TARGET
# make $TARGET && srun -p tut -t 720 --gres=gpu:1 ./$TARGET

srun -p interactive make $TARGET

# for I in `seq 1 10`; do
#   srun -p interactive -t 720 -n 1 -c $THR ./$TARGET
# done

# N_THR='1 2 3 4 5 6 6 8 10 12 14 16 20 24 28 32 36 40 44 48'
# N_CALLS=`seq 1 10`

# make $TARGET
> logs/logy.dat
# for I_THR in $N_THR; do
#   echo '====' $I_THR 'threads' '====' #>> run_multi.log
#   export OMP_NUM_THREADS=$I_THR
#   for I_CALL in $N_CALLS; do
#     echo '====' 'run' $I_CALL '====' #>> run_multi.log
#     srun -p tut -t 720 -n 1 -c $I_THR ./$TARGET $I_THR >> logx.dat
    srun -p tut -t 720 -n 1 -c $THR ./$TARGET $I_THR >> logs/logy.dat
#   done
# done
