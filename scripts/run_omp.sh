#!/bin/sh

TARGET=test_omp
THR=48

>logs/log_03022017.dat

export OMP_NUM_THREADS=$THR
make $TARGET && srun -p tut -t 720 -n 1 -c $THR ./$TARGET > logs/log_03022017.dat 2>&1
