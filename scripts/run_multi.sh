#!/bin/sh

TARGET=test_TIsobarJpsiToKKpi0Fit
N_THR='1 2 4 6 8 12 16 20 24 32 40 48'
# N_THR='8'
N_CALLS='0 1 2 3 4 5 6 7 8 9'

make $TARGET
> logs/logx.dat
for I_THR in $N_THR; do
  echo '====' $I_THR 'threads' '====' #>> run_multi.log
  export OMP_NUM_THREADS=$I_THR
  for I_CALL in $N_CALLS; do
#     echo '====' 'run' $I_CALL '====' #>> run_multi.log
    srun -p tut -t 720 -n 1 -c $I_THR ./$TARGET $I_THR >> logs/logx.dat 2>&1
  done
done
