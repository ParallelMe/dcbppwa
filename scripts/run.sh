#!/bin/bash

TARGET=test_JpsiToKKpi0 #executable
FRDIR="/nfs/hybrilit.jinr.ru/user/v/vtokareva/parallel_pwa/pwa_fumili"    #project directory

#logs stuff
LOGNAME=${FRDIR}/logs/log_"$(date +"%Y%m%d%H%M")".dat
> $LOGNAME

cd ${FRDIR}

. scripts/utils.sh  #add utils

#set up the omp threads
THR=24
export OMP_NUM_THREADS=$THR

inFrame "Current dir: ${PWD}"

inFrame "Making of the executable......"
srun -p interactive make $TARGET

#set up a device
choseDev
# dev="gpu"

#running...
case $dev in
     cpu)
          runMes "cpu"
          srun -p interactive -t 720 -n 1 -c $THR ./$TARGET 1>$LOGNAME 2>&1
          ;;
     gpu)
          runMes "gpu"
          ;;
     phi)
          runMes "phi"
          ;;
     *)
          echo -e "there is no such device\nExtining..."
          exit 1
          ;;
esac

#output message
inFrame "Results of running has written in \n$LOGNAME";


