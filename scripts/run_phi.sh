#!/bin/bash

TARGET=test_JpsiToKKpi0

THR=2
export OMP_NUM_THREADS=$THR

srun -p interactive make $TARGET

# N_THR='1 4 8 12 16 20 24 32 40 48 56 64 80 96 112 128 160 192 224'
N_THR='80'
# N_CALLS=`seq 1 10`

make $TARGET
# > logs/logw.dat
for I_THR in $N_THR; do
  echo '====' $I_THR 'threads' '====' #>> run_multi.log
#   export OMP_NUM_THREADS=$I_THR
#   for I_CALL in $N_CALLS; do
#     echo '====' 'run' $I_CALL '====' #>> run_multi.log
    srun -p interactive -t 720 -n 1 -c $THR ./$TARGET $I_THR #>> logs/logw.dat
#   done
done
