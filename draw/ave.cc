
#include <iostream>
#include <fstream>
using namespace std;

int main() {
  ifstream ifs("../log_phi.dat");
  ofstream ofs("ave_phi.dat");
  int prev_thr = -1;
  double time_sum = 0;
  int n = 0;
  while(true) {
    int n_thr = -1;
    double time;
    ifs>>n_thr>>time;
    if(n_thr == -1) break;
    if(n_thr != prev_thr && prev_thr != -1) {
      ofs<<prev_thr<<' '<<time_sum/n<<endl;
      time_sum = 0;
      n = 0;
    }
    prev_thr = n_thr;
    time_sum += time;
    n++;
    cout<<n_thr<<' '<<time<<' '<<prev_thr<<' '<<time_sum<<' '<<n<<endl;
  }
  ofs<<prev_thr<<' '<<time_sum/n<<endl;
  ifs.close();
  ofs.close();
}
