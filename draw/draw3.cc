
#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TString.h>
#include <TAxis.h>
#include <cstdlib>

// const TString NAME = "log_knl_18042017.dat";//"../test_JpsiToKKpi0_cpu.res";
// const TString PREFIX = "knl";//"cpu";
const TString NAME = "ave_phi.dat";//"../test_JpsiToKKpi0_cpu.res";
const TString PREFIX = "phi_n";//"cpu";

class Parse {
  std::ifstream ifs;
  int thr, thr0;
  double tot, tot0;
  void ReadLine();
  void PushGraph(TGraph * g, double y);
  void PushGraphs();
  void DoParse();
  void Draw();
  void DrawGraph(TGraph * g, const char * yAxis);
  double ReadNum();
  TGraph * gtot, * gacc, * geff;
public:
  Parse(const TString & s);
  ~Parse();
  static void SetStyle();
};
int main() {
  Parse::SetStyle();
  /// parsing file
//  const TString NAME = "all240";
 // const TString NAME = "code.16.06.30.16.16.53";
  Parse p(NAME);
}
Parse::Parse(const TString & s): thr(0), thr0(0), tot0(0) {
  ifs.open(s);
  if(!ifs.is_open()) {
    std::cerr<<"Cannot open file "<<s<<" for reading! Exiting..."<<std::endl;
    return;
  }
  gtot = new TGraph;
  gtot->SetName("gtot");
  gtot->SetMarkerStyle(20);
  gacc = new TGraph;
  gacc->SetName("gacc");
  gacc->SetMarkerStyle(20);
  geff = new TGraph;
  geff->SetName("geff");
  geff->SetMarkerStyle(20);
  DoParse();
  Draw();
}
void Parse::DoParse() {
  bool first = true;
  while(!ifs.eof()) {
    thr = -1;
    ifs>>thr>>tot;
    if(thr == -1) break;
    if(first) {
      first = false;
      thr0 = thr;
      tot0 = tot;
    }
    PushGraphs();
  }
}
void Parse::PushGraph(TGraph * g, double y) {
//   y /= 60.;
  std::cout<<' '<<g<<std::flush;
  std::cout<<' '<<g->GetN()<<' '<<thr<<' '<<y<<std::endl;
  g->SetPoint(g->GetN(), thr, y);
}
void Parse::PushGraphs() {
  PushGraph(gtot, tot/60.);
  PushGraph(gacc, tot0/tot);
  PushGraph(geff, (tot0*thr0)/(tot*thr));
}
void Parse::DrawGraph(TGraph * g, const char * yAxis) {
  g->SetMarkerSize(2);
  g->SetLineWidth(2);
  g->GetXaxis()->SetTitle("#threads");
  g->GetYaxis()->SetTitle(yAxis);
  g->GetXaxis()->SetNdivisions(50210);
  g->GetYaxis()->SetNdivisions(510);
  g->SetLineColor(kGreen+2);
  g->SetMarkerColor(kCyan+2);
  g->GetXaxis()->SetLabelColor(kOrange+2);
  g->GetXaxis()->SetTitleColor(kRed+2);
  g->GetYaxis()->SetLabelColor(kOrange+2);
  g->GetYaxis()->SetTitleColor(kRed+2);
  g->Draw("alp");
  g->SetMinimum(0);
}
void Parse::Draw() {
  TCanvas * c = new TCanvas("c", "c", 1000, 700);
  DrawGraph(gtot, "Time [min]");
  c->Update();
  c->SaveAs(PREFIX+"_time_col.eps");
//   gio->Draw("lp");
//   gcalc->Draw("lp");
  DrawGraph(gacc, "Speedup");
  c->Update();
  c->SaveAs(PREFIX+"_acc_col.eps");
  DrawGraph(geff, "Efficiency");
  c->Update();
  c->SaveAs(PREFIX+"_eff_col.eps");
  c->Close();
}
Parse::~Parse() {
  delete gtot;
  delete gacc;
  delete geff;
}
void Parse::SetStyle() {
  /// style
  gStyle->SetCanvasColor(kWhite);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetPadColor(kWhite);
  gStyle->SetPadBorderMode(0);
  gStyle->SetFrameFillColor(kWhite);
  gStyle->SetFrameBorderMode(0);
  gStyle->SetTitleFillColor(kWhite);
  gStyle->SetTitleFont(42);
  gStyle->SetLabelFont(42);
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(111);
  gStyle->SetStatColor(kWhite);
  gStyle->SetPadGridX(0);
  gStyle->SetPadGridY(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadLeftMargin(0.13/*0.12*/);
  gStyle->SetPadRightMargin(0.02/*0.13*/);
  gStyle->SetPadBottomMargin(0.15/*0.14*/);
  gStyle->SetPadTopMargin(0.03/*0.06*/);
  gStyle->SetStatFont(42);
  gStyle->SetTitleFont(42,"");
  gStyle->SetTitleFont(42,"X");
  gStyle->SetTitleFont(42,"Y");
  gStyle->SetTitleFont(42,"Z");
  gStyle->SetLabelFont(42,"X");
  gStyle->SetLabelFont(42,"Y");
  gStyle->SetLabelFont(42,"Z");
  gStyle->SetTitleSize(0.08, "X");
  gStyle->SetTitleSize(0.08, "Y");
  gStyle->SetTitleSize(0.08, "Z");
  gStyle->SetLabelSize(0.06, "X");
  gStyle->SetLabelSize(0.06, "Y");
  gStyle->SetLabelSize(0.06, "Z");
  gStyle->SetTitleOffset(0.8, "X");
  gStyle->SetTitleOffset(0.7, "Y");
  /// ===============================
}
