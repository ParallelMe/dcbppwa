
#include "TPWAFunctionGPU.h"
#include "TDataCache.h"
#include "TCalcGPU.h"
#include "ConfigParser.h"
#include <cstring>
#include <iostream>
using std::cerr;
using std::endl;
#include <omp.h>

// int TPWAFunction::verbosity = 1;
// double TPWAFunction::grad_step = 1e-5;

void TPWAFunctionGPU::NCaches() {
  n_caches = 1;
  omp_set_num_threads(n_caches);
}

TCalcCache * TPWAFunctionGPU::CreateCalcCache(TCache * x, unsigned offset, unsigned blockSize) {
  TCalcCache * tmp = new TCalcGPU(x, offset, blockSize);
  mem_alloc_gpu += tmp->mem_alloc;
  return tmp;
}

void TPWAFunctionGPU::PrintMem(unsigned _nev) {
  TCache::PrintMem(mem_alloc, resonances.N(), _nev);
  TCache::PrintMem(mem_alloc_gpu, resonances.N(), _nev, "GPU");
}

TPWAFunctionGPU::TPWAFunctionGPU(string ConfigFileName) {
  Init(ConfigFileName);
}
