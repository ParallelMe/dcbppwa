
#supported compilers
CXX     := g++
NVCC	:= nvcc
ICC	:=icpc

#info lambda functions
CXX_INFO := $(shell which $(CXX) >/dev/null 2>&1; echo $$?)
NVCC_INFO := $(shell which $(NVCC) >/dev/null 2>&1; echo $$?)
ICC_INFO := $(shell which $(ICC) >/dev/null 2>&1; echo $$?)

#include path
IDIR	:= include

#target name
TARGET	:= test_JpsiToKKpi0
SUBTARGET := info

#g++ flags
CXXFLAGS := -O2 -g -fPIC -std=c++0x -Wall -fopenmp
CXXFLAGS += -I$(IDIR)
CXXFLAGS += $(shell root-config --cflags)

#icpc flags
ICCFL	:= $(CXXFLAGS)
ICCFL	+= -qopt-report-phase=vec -qopt-report=5

#g++ and icpc linking flags
LDFLAGS  := -O2 -fopenmp  
LDFLAGS  += $(shell root-config --libs) -lMinuit -lFumili
ifeq ($(NVCC_INFO),0)
LDFLAGS  += -lcudart
endif

#nvcc flags for compilation and linking
NVFLAGS  := -O2 -g -arch=compute_30
NVFLAGS  += -I$(IDIR)

# NVCC_INFO := 1

#object we compile and link anyway
OBJS := VTVector3 VTLorentzVector multiindex TX TVertex1to2 TEpsilon
OBJS += TResonanceSet ConfigParser TCache TCacheFile TCalcCache
OBJS += TJpsiToKKpi0Const TDataCache TCalcCPU
OBJS += TPWAFunction Minimizer $(TARGET)

# #specail gpu objects (kernels)
# ifeq ($(NVCC_INFO),0)
# OBJS += Kernels GPUMemoryHandler TCalcGPU gpuCode
# SUBTARGET += GPUtest
# endif
# 
ifeq ($(ICC_INFO),0)
SUBTARGET += PHItest
OBJS += TCalcPHI
endif

ifeq ($(CXX_INFO),0)
SUBTARGET += CPUtest
endif

#linking main target
$(TARGET): $(SUBTARGET)
	@echo "Linking available targets..."
	
#test target
info:	
	@#test if compilers are available (0 - true, 1 - false)
	@echo "Compiler info: "
	@echo "g++:"$(CXX_INFO)
	@echo "nvcc:"$(NVCC_INFO)
	@echo "icpc:"$(ICC_INFO)
	@echo "=============================================================================================="
	@#test comilation flags difference
	@echo "g++ cflags: "$(CXXFLAGS)
	@echo "icpc cflags: "$(ICCFL)
	@echo "nvcc cflags: "$(NVFLAGS)
	@echo "=============================================================================================="
	@#test objects list
	@echo "Objects : "$(OBJS)
	@echo "=============================================================================================="
	
#CPUtest:
#linking...
CPUtest:$(addprefix buildCPU/,$(addsuffix .o,$(OBJS)))
	@echo "Linking $@..."
	@$(CXX) $^ $(LDFLAGS) -o $@

#compilation...
buildCPU/%.o: src/%.cpp buildCPU/%.d
	@echo "Compiling $<..."
	@$(CXX) $< $(CXXFLAGS) -c -o $@
	
#dependencies
buildCPU/%.d: src/%.cpp
	@echo "Generating dependencies for $<..."
	@$(CXX) $< -MM $(CXXFLAGS) -MT $(subst .d,.o,$@) -o $@	
	
#PHItest:
#linking...
PHItest:$(addprefix buildPHI/,$(addsuffix .o,$(OBJS)))
	@echo "Linking $@..."
	@$(ICC) $^ $(LDFLAGS) -o $@

#compilation....


#dependencies...



# 	
# GPUtest: $(addprefix buildCPU/,$(addsuffix .o,$(OBJS))) $(addprefix buildGPU/,$(addsuffix .o,$(OBJS)))
# 
# 
# buildGPU/gpuCode.o: $(addprefix build/,$(addsuffix .o,$(OBJSGPU)))
# 	@echo "Linking GPU code..."
# 	@$(NVCC) $^ $(NVFLAGS) -dlink -o $@
# 
# %: build/%.o
# 	@echo "Linking $@..."
# 	@$(CXX) $^ $(LDFLAGS) -o $@
# 	
# ####### Compile
# 
# build/%.o: src/%.cpp build/%.d
# 	@echo "Compiling $<..."
# 	@$(CXX) $< $(CXXFLAGS) -c -o $@
# 
# # ifeq ($(NVCC_INFO),0)
# # build/%.o: src/%.cu build/%.d
# # 	@echo "Compiling $<..."
# # 	@$(NVCC) $< $(NVFLAGS) -dc -o $@
# # endif
# 
# ####### Dependencies
# 
# build/%.d: src/%.cpp
# 	@echo "Generating dependencies for $<..."
# 	@$(CXX) $< -MM $(CXXFLAGS) -MT $(subst .d,.o,$@) -o $@
# 
# # ifeq ($(NVCC_INFO),0)
# # build/%.d: src/%.cu
# # 	@echo "Generating dependencies for $<..."
# # 	@$(NVCC) $< -M $(NVFLAGS) -MT $(subst .d,.o,$@) -o $@
# # endif

clean:
	@echo "Cleaning optimization reports..."
	@$(RM) build/*.optrpt
	@echo "Cleaning dependencies..."
	@$(RM) buildCPU/*.o
	@echo "Cleaning object files..."
	@$(RM) buildCPU/*.d
	@echo "Cleaning executables..."
	@$(RM) $(SUBTARGET)

include $(wildcard build/*.d)

.PRECIOUS: buildCPU/%.d

.PHONY: clean
